package worker.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import worker.config.ContantConfig;
import worker.messages.ActionSent;
import worker.messages.UpdateNotify;
import worker.modal.Config;
import worker.modal.MapModal;
import worker.modal.WorkerDetail;
import worker.modal.WorkerModal;
import worker.util.ApacheRestClient;
import worker.util.RestClient;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SimulatorHelper {

    private HashMap<String, String> notificationArray = new HashMap<String, String>();

    ObjectMapper mapper = new ObjectMapper();
    RestClient restClient = new RestClient();
    ApacheRestClient restClientApache = new ApacheRestClient();

    ExecutorService executor = Executors.newFixedThreadPool(100);

    ContantConfig myContants = new ContantConfig();


    public HashMap<String, String> getNotificationArray() {
        return notificationArray;
    }

    public void setNotificationArray(HashMap<String, String> notificationArray) {
        this.notificationArray = notificationArray;
    }

    public HashMap<String, String> getWorkers() {
        try {

            System.out.println("Sending to: " + myContants.getSimulator_getWorkers());

            worker.util.WebResponse response = restClient.get(myContants.getSimulator_getWorkers());
            if (response == null) System.out.println("working response");
            if (response != null)
                System.out.println("working response------" + response.getPayload() + "----" + response.getCode());

            //convert JSON array to Array objects
            WorkerModal[] newWorker = mapper.readValue(response.getPayload(), WorkerModal[].class);


            for (WorkerModal thisWorker : newWorker) {
                this.notificationArray.put(thisWorker.getName(), myContants.getNotifyUrl() + thisWorker.getName());
            }

            for (Map.Entry m : this.notificationArray.entrySet()) {
                System.out.println(m.getKey() + " " + m.getValue());
            }
        } catch (IOException e) {
            System.out.println("Error--" + e.getMessage());
        } finally {
            return notificationArray;
        }
    }

    public void updateWorkerNotiUrls() {

        for (Map.Entry m : this.notificationArray.entrySet()) {

            executor.execute(new Runnable() {
                @Override
                public void run() {
                    try {

                        String url = myContants.getSimulator_notifyUrl() + m.getKey();
                        String jsonMsg = mapper.writeValueAsString(new UpdateNotify(m.getValue().toString()));

                        int response = restClientApache.patch(url, RestClient.JSON_TYPE, jsonMsg);

                        if (response != 0) {
                            System.out.println("working response-------updated notify-" + m.getKey() + "---" + response);
                        }


                    } catch (Exception e) {
                        System.out.println("exception----------");

                    }
                }
            });

        }

    }


    public WorkerDetail getWorkerByID(String id) {

        try {
            String url = myContants.getSimulator_notifyUrl() + id;
            System.out.println("Sending to: " + url);

            worker.util.WebResponse response = restClient.get(url);
            if (response == null) System.out.println("working response");
            if (response != null)
                System.out.println("working response------" + response.getPayload() + "----" + response.getCode());

            //convert JSON array to Array objects
            WorkerDetail newWorker = mapper.readValue(response.getPayload(), WorkerDetail.class);

            System.out.println("worker detail--" + newWorker.getName());
            return newWorker;

//            return new WorkerDetail();

        } catch (Exception e) {
            System.out.println("exception occuered worker ID");
            return new WorkerDetail();
        }


    }

    public MapModal getMapDetails() {

        try {
            String url = myContants.getSimulator_mapUrl();
            System.out.println("Sending to: " + url);

            worker.util.WebResponse response = restClient.get(url);
            if (response == null) System.out.println("working response");
            if (response != null)
                System.out.println("working response------" + response.getPayload() + "----" + response.getCode());

            //convert JSON array to Array objects
            MapModal mapModal = mapper.readValue(response.getPayload(), MapModal.class);

            return mapModal;

//            return new WorkerDetail();

        } catch (Exception e) {
            System.out.println("exception occuered- map");
           return new MapModal();
        }
    }


    public Config getConfigDetails() {

        try {
            String url = myContants.getSimulator_configUrl();
            System.out.println("Sending to: " + url);

            worker.util.WebResponse response = restClient.get(url);
            if (response == null) System.out.println("working response");
            if (response != null)
                System.out.println("working response------" + response.getPayload() + "----" + response.getCode());

            //convert JSON array to Array objects
            Config config = mapper.readValue(response.getPayload(), Config.class);

//            System.out.println("payload---"+newWorker);

//            System.out.println("worker detail--" + mapModal.getId());
            return config;

//            return new WorkerDetail();

        } catch (Exception e) {
            System.out.println("exception occuered config");
            return new Config();
        }
    }


    public Boolean sendNextAction(String user, ActionSent actionSent) {

        try {
            String url = myContants.getSimulator_notifyUrl() + user + "/nextAction";
            System.out.println("Sending to: " + url);

            String json = mapper.writeValueAsString(actionSent);

            worker.util.WebResponse response = restClient.put(url,RestClient.JSON_TYPE,json);
            if (response == null) System.out.println("working response");
            if (response != null)
                System.out.println("working response-- nextaction----" + response.getPayload() + "----" + response.getCode());

            //convert JSON array to Array objects
//            Config config = mapper.readValue(response.getPayload(), Config.class);

//            System.out.println("payload---"+newWorker);

//            System.out.println("worker detail--" + mapModal.getId());
            return true;

//            return new WorkerDetail();

        } catch (Exception e) {
            System.out.println("exception occuered next action");
            return false;
        }
    }




}
