package worker.service;

import worker.messages.ActionSent;
import worker.messages.ActorOrder;
import worker.modal.Config;
import worker.modal.ItemModal;
import worker.modal.OrdersModal;
import worker.modal.WorkerDetail;

import java.util.ArrayList;
import java.util.List;

public class OrderHelper {

    SimulatorHelper simHelper = new SimulatorHelper();

    public List<String> processOrders(ActorOrder msg) {

        WorkerDetail workerDetail = simHelper.getWorkerByID((String) msg.getName());
        System.out.println("Second: works - on orders--" + workerDetail.getName() + "-- " + workerDetail.getCapacity());

        Config config = simHelper.getConfigDetails();
        System.out.println("Second: config - on orders--" + config.getAisles());

        GraphHelper myGraph = new GraphHelper();

        List<OrdersModal> myOrders = msg.getActor().getOrders();

        ItemModal[] currentItemList = myOrders.get(0).getItems();
        String currentOrderWeight = myOrders.get(0).getWeight();

        List<String> pathList = new ArrayList<>();

        if (Integer.parseInt(currentOrderWeight) < workerDetail.getCapacity()) {

            //finding path to collect all order items
            for (int i = 0; i < currentItemList.length; i++) {
                if (i == 0) {
                    String itemLocation = currentItemList[i].getLocation().split("/")[0];
                    List<String> path = myGraph.getShortestPath(workerDetail.getLocation(), itemLocation);
                    pathList.addAll(path);
                } else if (i > 0) {
                    String itemLocation = currentItemList[i].getLocation().split("/")[0];
                    List<String> path = myGraph.getShortestPath(pathList.get(pathList.size() - 1), itemLocation);
                    pathList.addAll(path);
                }
            }

            //finding path to the packing station
            List<String> packagingAreas = config.getPackagingAreas();
            List<String> packingPath = new ArrayList<>();

            for (String x : packagingAreas){
                List<String> path = myGraph.getShortestPath(pathList.get(pathList.size() - 1), x);
                //replace packing path with current path as it is shorter
                if(packingPath.size() == 0 || packingPath.size() > path.size()){
                    packingPath.clear();
                    packingPath.addAll(path);
                }
            }

            pathList.addAll(packingPath);
        }

        return pathList;

    }

    public ActionSent getNextStep(List<String> pathList) {

        String type = "";
        String[] argument = {pathList.get(0)};

        //if reach end of path
        if(pathList.size() == 1){
            type = "PACK";
        }else{
            String myStep = pathList.get(0);
            if(myStep.equals(pathList.get(1))) type = "PICK";
            else type = "MOVE";
        }



        ActionSent actionSent = new ActionSent(type,argument);

        return actionSent;
    }

}
