package worker.service;

import org.jgrapht.Graph;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import worker.modal.MapModal;

import java.util.HashMap;
import java.util.List;

public class GraphHelper {

    private static MapModal mapModal;
    private static Graph<String, DefaultEdge> graph;


    public GraphHelper() {
        SimulatorHelper simHelper = new SimulatorHelper();
        this.mapModal = simHelper.getMapDetails();
        this.graph = setGraphEdpoints(mapModal);

    }


    public Graph<String, DefaultEdge> setGraphEdpoints(MapModal mapModal) {
        Graph<String, DefaultEdge> g
                = new SimpleGraph<>(DefaultEdge.class);

        for (String i : mapModal.getVertices()) {

            g.addVertex(i);
        }
        for (String i : mapModal.getEdges()) {
            String vertex1 = i.split("->")[0].trim();
            String vertex2 = i.split("->")[1].trim();

            g.addEdge(vertex1, vertex2);
        }

        return g;
    }

    public List<String> getShortestPath(String vertexSource, String vertexTraget) {
        DijkstraShortestPath dijkstraShortestPath
                = new DijkstraShortestPath(graph);
        List<String> shortestPath = dijkstraShortestPath
                .getPath(vertexSource, vertexTraget).getVertexList();

        return shortestPath;

    }


    public void breadthFirstSearch(String[] v) {

        //Label each vertex as UNEXPLORED
        String[] unexplored = v;

        //Create a hash map, M
//        HashMap<> m = new HashMap();


    }
}
