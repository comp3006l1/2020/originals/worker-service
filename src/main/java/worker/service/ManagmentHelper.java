package worker.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import worker.config.ContantConfig;
import worker.messages.Result;
import worker.util.RestClient;

public class ManagmentHelper {

    ObjectMapper mapper = new ObjectMapper();
    RestClient restClient = new RestClient();

    ContantConfig myContants = new ContantConfig();

    public Boolean updateOrderStatus(String orderID, Result ackNotify) {
        try {
            String url = myContants.getManagment_order() + orderID ;
            System.out.println("Sending to: " + url);

            String json = mapper.writeValueAsString(ackNotify);

            worker.util.WebResponse response = restClient.put(url,RestClient.JSON_TYPE,json);
            if (response == null) System.out.println("working response");
            if (response != null)
                System.out.println("working response-- nextaction----" + response.getPayload() + "----" + response.getCode());

            //convert JSON array to Array objects
//            Config config = mapper.readValue(response.getPayload(), Config.class);

//            System.out.println("payload---"+newWorker);

//            System.out.println("worker detail--" + mapModal.getId());
            return true;

//            return new WorkerDetail();

        } catch (Exception e) {
            System.out.println("exception occuered next action");
            return false;
        }
    }
}
