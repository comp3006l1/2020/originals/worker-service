package worker.config;

public class ContantConfig {

    private final int port = 6000;
    private final String notifyUrl = "http://localhost:6000/notification/";

    private final String workUrl = "http://localhost:6000/workers/";

    private final String rest_postNotify = "notification";

    private final String rest_managment = "workers";

    private final String simulator_getWorkers = "http://localhost:8300/workers";
    private final String simulator_notifyUrl = "http://localhost:8300/workers/";

    private final String simulator_configUrl = "http://localhost:8300/configuration";

    private final String simulator_mapUrl = "http://localhost:8300/map";

    private final String managment_order = "https://managmentservice.herokuapp.com/order/";

    public String getManagment_order() {
        return managment_order;
    }

    public String getSimulator_configUrl() {
        return simulator_configUrl;
    }


    public String getSimulator_mapUrl() {
        return simulator_mapUrl;
    }


    public int getPort() {
        return port;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public String getRest_postNotify() {
        return rest_postNotify;
    }

    public String getSimulator_getWorkers() {
        return simulator_getWorkers;
    }

    public String getSimulator_notifyUrl() {
        return simulator_notifyUrl;
    }

    public String getWorkUrl() {
        return workUrl;
    }

    public String getRest_managment() {
        return rest_managment;
    }

}
