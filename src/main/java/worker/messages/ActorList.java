package worker.messages;



import worker.modal.ActorDetail;

import java.util.HashMap;

public class ActorList {

    private static HashMap<String, ActorDetail> actorslist;

    public ActorList(){}

    public ActorList(HashMap<String, ActorDetail> actorslist) {
        this.actorslist= actorslist;
    }


    public HashMap<String, ActorDetail> getActorslist() {
        return actorslist;
    }

    public void setActorslist(HashMap<String, ActorDetail> actorslist) {
        this.actorslist = actorslist;
    }
}
