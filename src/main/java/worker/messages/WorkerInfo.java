package worker.messages;

import worker.modal.WorkerDetail;

public class WorkerInfo {

    private String url;
    private WorkerDetail worker;

    public WorkerInfo() {}

    public WorkerInfo(String url, WorkerDetail worker) {
        this.worker = worker;
        this.url = url;

    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public WorkerDetail getWorker() {
        return worker;
    }

    public void setWorker(WorkerDetail worker) {
        this.worker = worker;
    }


}
