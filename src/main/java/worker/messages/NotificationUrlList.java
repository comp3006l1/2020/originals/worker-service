package worker.messages;

import java.util.HashMap;

public class NotificationUrlList {

    private HashMap<String,String> notificationList;

    public NotificationUrlList(){}

    public NotificationUrlList(HashMap<String,String> notificationList) {
        this.notificationList= notificationList;
    }

    public HashMap<String, String> getNotificationList() {
        return notificationList;
    }

    public void setNotificationList(HashMap<String, String> notificationList) {
        this.notificationList = notificationList;
    }




}
