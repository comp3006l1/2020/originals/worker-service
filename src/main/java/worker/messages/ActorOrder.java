package worker.messages;

import akka.actor.ActorRef;
import worker.modal.ActorDetail;

public class ActorOrder {

    private ActorDetail actor;
    private String name;



    public ActorOrder(ActorDetail actor, String name) {
        this.actor = actor;
        this.name = name;
    }

    public ActorDetail getActor() {
        return actor;
    }

    public void setActor(ActorDetail actor) {
        this.actor = actor;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
