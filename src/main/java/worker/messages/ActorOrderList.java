package worker.messages;

import java.util.HashMap;

public class ActorOrderList {

    private static HashMap<String, ActorOrder> actorsOrderList;

    public static HashMap<String, ActorOrder> getActorsOrderList() {
        return actorsOrderList;
    }

    public static void setActorsOrderList(HashMap<String, ActorOrder> actorsOrderList) {
        ActorOrderList.actorsOrderList = actorsOrderList;
    }
}
