package worker.messages;

public class UpdateNotify {

    private String notificationUri;

    public UpdateNotify() {
    }

    public UpdateNotify(String notificationUri) {
        this.notificationUri = notificationUri;
    }

    public String getNotificationUri() {
        return notificationUri;
    }

    public void setNotificationUri(String notificationUri) {
        this.notificationUri = notificationUri;
    }

}
