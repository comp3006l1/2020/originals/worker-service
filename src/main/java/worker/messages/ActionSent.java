package worker.messages;

public class ActionSent {

    private String type;
    private String[] arguments;

    public ActionSent() {}

    public ActionSent(String type, String[] arguments) {
        this.type = type;
        this.arguments = arguments;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String[] getArguments() {
        return arguments;
    }

    public void setArguments(String[] arguments) {
        this.arguments = arguments;
    }

}
