package worker.messages;

import worker.modal.OrdersModal;

public class OrderReq {


    private OrdersModal order;

    public OrdersModal getOrder() {
        return order;
    }

    public void setOrder(OrdersModal order) {
        this.order = order;
    }
}
