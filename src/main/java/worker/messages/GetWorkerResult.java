package worker.messages;



public class GetWorkerResult {


    private boolean success;
    private WorkerInfo[] data;

    public GetWorkerResult() {

    }

    public GetWorkerResult(boolean success,WorkerInfo[] data ) {
        this.success = success;
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public WorkerInfo[] getData() {
        return data;
    }

    public void setData(WorkerInfo[] data) {
        this.data = data;
    }
}

