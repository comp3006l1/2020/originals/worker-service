package worker.messages;

import worker.modal.ActorDetail;

public class ActorNotify {



    private ActorDetail actorDetail;
    private String name;

    public ActorNotify() {}
    public ActorNotify(ActorDetail actorDetail , String name) {
        this.actorDetail = actorDetail;
        this.name = name;
    }


    public ActorDetail getActorDetail() {
        return actorDetail;
    }

    public void setActorDetail(ActorDetail actorDetail) {
        this.actorDetail = actorDetail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
