package worker.messages;

import worker.modal.OrdersModal;

public class WorkerOrder {


    private String name;
    private OrdersModal orders;

    public WorkerOrder() {
    }

    public WorkerOrder(String name, OrdersModal orders) {
        this.name = name;
        this.orders = orders;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OrdersModal getOrders() {
        return orders;
    }

    public void setOrders(OrdersModal orders) {
        this.orders = orders;
    }
}
