package worker.messages;

public class NotifyWorker {


    private String name;
    private ActionReceive data;

    public NotifyWorker(String name, ActionReceive data) {
        this.name = name;
        this.data = data;
    }

    public ActionReceive getData() {
        return data;
    }

    public void setData(ActionReceive data) {
        this.data = data;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
