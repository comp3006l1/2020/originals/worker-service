package worker.util;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.UnsupportedEncodingException;
import java.net.URI;

public class ApacheRestClient {
    public int patch(String url, String type, String content) throws UnsupportedEncodingException {
        return sendPatchRequest( url, type, content.getBytes("utf-8"));
    }

    public int sendPatchRequest( String url, String type, byte[] content) {

        try {
            CloseableHttpClient httpClient = HttpClients.createDefault();
            HttpPatch httpPatch = new HttpPatch(new URI(url));
            if (type != null) {
                httpPatch.setHeader("Content-Type", type);
            }
            HttpEntity httpEntity = new ByteArrayEntity(content);
            httpPatch.setEntity(httpEntity);


            CloseableHttpResponse response = httpClient.execute(httpPatch);
            return response.getStatusLine().getStatusCode();

        } catch (Exception e) {
            return 500;
        }

    }
}
