package worker;

import akka.actor.AbstractActor;
import akka.actor.Props;
import worker.messages.*;
import worker.modal.*;
import worker.service.ManagmentHelper;
import worker.service.OrderHelper;
import worker.service.SimulatorHelper;

import java.util.HashMap;
import java.util.List;


public class Worker extends AbstractActor {
    public static Props props() {
        return Props.create(Worker.class);
    }


    SimulatorHelper simHelper = new SimulatorHelper();
    OrderHelper orderHelper = new OrderHelper();
    ActorList actorList = new ActorList();
    ManagmentHelper managmentHelper = new ManagmentHelper();

    @Override
    public Receive createReceive() {
        return receiveBuilder().
                match(ActorOrder.class,
                        msg -> {
                            System.out.println("Second: works - on orders--" + msg.getName());

                            //get full path list
                            List<String> pathList = orderHelper.processOrders(msg);

                            for (String i : pathList) {
                                System.out.println("path list -- " + i);
                            }

                            ActionSent actionSent = orderHelper.getNextStep(pathList);

//                            ActionSent to the simulator
                            Boolean flag = simHelper.sendNextAction(msg.getName(), actionSent);

                            if (flag) {

                                //remove completed path from path list
                                pathList.remove(0);

                                ActorDetail actorDetail = actorList.getActorslist().get((String) msg.getName());
                                List<OrdersModal> newOrderList = actorDetail.getOrders();
                                OrdersModal newOrder = newOrderList.get(0);
                                newOrder.setPath(pathList); // add new path list to order modal

                                newOrderList.set(0, newOrder);
                                actorDetail.setOrders(newOrderList);

                                HashMap<String, ActorDetail> newActorList = actorList.getActorslist();
                                newActorList.put(msg.getName(), actorDetail);

                                actorList.setActorslist(newActorList);
                            }

                        }).
                match(ActorNotify.class,
                        msg -> {

                            ActorDetail actorDetail = actorList.getActorslist().get((String) msg.getName());
                            List<OrdersModal> ordersModalList = actorDetail.getOrders();
                            OrdersModal ordersModal = ordersModalList.get(0);

                            List<String> pathList = ordersModal.getPath();

                            ActionSent actionSent = orderHelper.getNextStep(pathList);

//                            ActionSent to the simulator
                            Boolean flag = simHelper.sendNextAction(msg.getName(), actionSent);

                            if(pathList.size() <= 1){
                                //path has completed
                                //so delete orderlist from ActorDetail and inform managmanet service

                                List<OrdersModal> newOrderList = actorDetail.getOrders();
                                //send order completed to managmnet
                                managmentHelper.updateOrderStatus(ordersModalList.get(0).getId(),new Result(true));

                                newOrderList.remove(0);
                                actorDetail.setOrders(newOrderList);

                                HashMap<String, ActorDetail> newActorList = actorList.getActorslist();
                                newActorList.put(msg.getName(), actorDetail);

                                actorList.setActorslist(newActorList);
                            }else if(flag) {
                                pathList.remove(0);

                                List<OrdersModal> newOrderList = actorDetail.getOrders();
                                OrdersModal newOrder = newOrderList.get(0);
                                newOrder.setPath(pathList); // add new path list to order modal

                                newOrderList.set(0, newOrder);
                                actorDetail.setOrders(newOrderList);

                                HashMap<String, ActorDetail> newActorList = actorList.getActorslist();
                                newActorList.put(msg.getName(), actorDetail);

                                actorList.setActorslist(newActorList);
                            }

                        }).
                build();
    }


}
