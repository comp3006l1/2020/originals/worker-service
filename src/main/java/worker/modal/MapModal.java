package worker.modal;

public class MapModal {
    private int id;
    private String[] vertices;
    private String[] edges;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String[] getVertices() {
        return vertices;
    }

    public void setVertices(String[] vertices) {
        this.vertices = vertices;
    }

    public String[] getEdges() {
        return edges;
    }

    public void setEdges(String[] edges) {
        this.edges = edges;
    }


}
