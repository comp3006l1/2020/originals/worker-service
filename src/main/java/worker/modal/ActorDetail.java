package worker.modal;

import akka.actor.ActorRef;

import java.util.ArrayList;
import java.util.List;

public class
ActorDetail {

    private ActorRef actor;
    private String notificationUri;
    private String workUri;
    private List<OrdersModal> orders = new ArrayList<>();


    public List<OrdersModal> getOrders() {
        return orders;
    }

    public void setOrders(List<OrdersModal> orders) {
        this.orders = orders;
    }

    public ActorRef getActor() {
        return actor;
    }

    public void setActor(ActorRef actor) {
        this.actor = actor;
    }

    public String getNotificationUri() {
        return notificationUri;
    }

    public void setNotificationUri(String notificationUri) {
        this.notificationUri = notificationUri;
    }

    public String getWorkUri() {
        return workUri;
    }

    public void setWorkUri(String workUri) {
        this.workUri = workUri;
    }
}
