package worker.modal;

import java.util.ArrayList;
import java.util.List;

public class WorkerDetail {

    private String name;
    private String location;
    private Integer capacity;
    private List<WorkerAction> actions;
    private WorkerAction nextAction;
    private Integer weight;
    private String notificationUri;
    private String[] holding;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNotificationUri() {
        return notificationUri;
    }

    public void setNotificationUri(String notificationUri) {
        this.notificationUri = notificationUri;
    }

    public WorkerAction getNextAction() {
        return nextAction;
    }

    public void setNextAction(WorkerAction nextAction) {
        this.nextAction = nextAction;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public List<WorkerAction> getActions() {
        return actions;
    }

    public void setActions(List<WorkerAction> actions) {
        this.actions = actions;
    }


    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }


    public String[] getHolding() {
        return holding;
    }

    public void setHolding(String[] holding) {
        this.holding = holding;
    }
}


class WorkerAction {


    private Integer id;
    private String type;
    private List<String> arguments;
    private boolean success;
    private String step;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getArguments() {
        return arguments;
    }

    public void setArguments(List<String> arguments) {
        this.arguments = arguments;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

}