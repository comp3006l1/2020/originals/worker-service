package worker.modal;

import java.util.List;

public class Config {


    private Integer id;
    private Integer aisles;
    private Integer sections;
    private Integer shelves;
    private List<String> packagingAreas;
    private WorkerDetail[] workers;
    private ItemModal[] items;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAisles() {
        return aisles;
    }

    public void setAisles(Integer aisles) {
        this.aisles = aisles;
    }

    public Integer getSections() {
        return sections;
    }

    public void setSections(Integer sections) {
        this.sections = sections;
    }

    public Integer getShelves() {
        return shelves;
    }

    public void setShelves(Integer shelves) {
        this.shelves = shelves;
    }

    public List<String> getPackagingAreas() {
        return packagingAreas;
    }

    public void setPackagingAreas(List<String> packagingAreas) {
        this.packagingAreas = packagingAreas;
    }

    public WorkerDetail[] getWorkers() {
        return workers;
    }

    public void setWorkers(WorkerDetail[] workers) {
        this.workers = workers;
    }

    public ItemModal[] getItems() {
        return items;
    }

    public void setItems(ItemModal[] items) {
        this.items = items;
    }
}
