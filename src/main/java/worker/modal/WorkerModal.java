package worker.modal;

import java.util.List;

public class WorkerModal {

    private String name;

    public WorkerModal(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}


