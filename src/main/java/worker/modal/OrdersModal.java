package worker.modal;

import java.util.ArrayList;
import java.util.List;

public class OrdersModal {

    private String id;
    private ItemModal[] items;
    private String status;
    private String weight;
    private List<String> path = new ArrayList<>();
    private boolean processing =  false; // 0 - not proccesed, 1 - processing , 2 - completed

    public List<String> getPath() {
        return path;
    }

    public void setPath(List<String> path) {
        this.path = path;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }


    public boolean isProcessing() {
        return processing;
    }

    public void setProcessing(boolean processing) {
        this.processing = processing;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ItemModal[] getItems() {
        return items;
    }

    public void setItems(ItemModal[] items) {
        this.items = items;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
