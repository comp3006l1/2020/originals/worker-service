package worker;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import com.fasterxml.jackson.databind.ObjectMapper;
import worker.config.ContantConfig;
import worker.messages.*;
import worker.modal.ActorDetail;
import worker.modal.OrdersModal;
import worker.modal.WorkerDetail;
import worker.modal.WorkerModal;
import worker.service.SimulatorHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class WorkerManager extends AbstractActor {
    public static Props props() {
        return Props.create(WorkerManager.class);
    }

    worker.util.RestClient restClient = new worker.util.RestClient();
    ObjectMapper mapper = new ObjectMapper();

    ContantConfig myContants = new ContantConfig();

    ActorList actorList = new ActorList();

    SimulatorHelper simHelper = new SimulatorHelper();

    @Override
    public Receive createReceive() {
        return receiveBuilder().
                match(Init.class,
                        msg -> {

                            HashMap<String, String> notiUrls = simHelper.getWorkers();

                            simHelper.updateWorkerNotiUrls();

                            HashMap<String, ActorDetail> actorslist = new HashMap<>();

                            //set actor array
                            for (Map.Entry m : notiUrls.entrySet()) {

                                //creating worker actor
                                ActorRef secondRef = getContext().actorOf(Worker.props(), (String) m.getKey());

                                //creating actordetail
                                ActorDetail actor = new ActorDetail();
                                actor.setActor(secondRef);
                                actor.setNotificationUri((String) m.getValue());
                                actor.setWorkUri(myContants.getWorkUrl() + (String) m.getKey());

                                //add actor and name to hashmap
                                actorslist.put((String) m.getKey(), actor);
                            }

                            actorList.setActorslist(actorslist);

                            getSender().tell(new NotificationUrlList(notiUrls), getSelf());

                        }).
                match(NotifyWorker.class,
                        msg -> {
                            System.out.println("working got got");

                            if (!actorList.getActorslist().isEmpty()) {

                                ActorDetail actorDetail = actorList.getActorslist().get((String) msg.getName());

                                actorDetail.getActor().tell(new ActorNotify(actorDetail, msg.getName()), getSelf());

                            }

                            getSender().tell(new Result(true), getSelf());
                        }).
                matchEquals("getWorker",
                        msg -> {
                            System.out.println("working got workeers");

//                            WorkerInfo[] workerArray;
                            List<WorkerInfo> workerArray = new ArrayList<WorkerInfo>();

                            if (actorList.getActorslist().isEmpty()) {
                                System.out.println(" --- get worker details initiated failed -- empty");
                            } else {
                                for (Map.Entry m : actorList.getActorslist().entrySet()) {
                                    System.out.println(m.getKey() + " --- get worker details initiated");
                                    //worker details
                                    WorkerDetail workerDetail = simHelper.getWorkerByID((String) m.getKey());

                                    //worker actor details
                                    ActorDetail actorDetail = (ActorDetail) m.getValue();

                                    //set to output worker format
                                    workerArray.add(new WorkerInfo(actorDetail.getWorkUri(), workerDetail));
                                }
                            }

                            WorkerInfo[] itemsArray = new WorkerInfo[workerArray.size()];
                            itemsArray = workerArray.toArray(itemsArray);


                            getSender().tell(new GetWorkerResult(true, itemsArray), getSelf());
                        }).
                match(WorkerOrder.class,
                        msg -> {
                            System.out.println("working order asign--" + msg.getName());


                            if (!actorList.getActorslist().isEmpty()) {

                                ActorDetail actorDetail = actorList.getActorslist().get((String) msg.getName());

                                System.out.println("working order actor--" + actorDetail.getActor() + "--" + actorDetail.getWorkUri());

                                List<OrdersModal> newOrderList = actorDetail.getOrders();

                                newOrderList.add(msg.getOrders());

                                actorDetail.setOrders(newOrderList);

                                HashMap<String, ActorDetail> newActorList = actorList.getActorslist();
                                newActorList.put(msg.getName(), actorDetail);

                                actorList.setActorslist(newActorList);

                                actorDetail.getActor().tell(new ActorOrder(actorDetail, msg.getName()), getSelf());

                            }


                            getSender().tell(new Result(true), getSelf());
                        }).
                build();
    }






}
