package worker;

import akka.NotUsed;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.http.javadsl.ConnectHttp;
import akka.http.javadsl.Http;
import akka.http.javadsl.marshallers.jackson.Jackson;
import akka.http.javadsl.model.HttpRequest;
import akka.http.javadsl.model.HttpResponse;
import akka.http.javadsl.model.StatusCodes;
import akka.http.javadsl.server.AllDirectives;
import akka.http.javadsl.server.Route;
import akka.pattern.Patterns;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Flow;
import akka.util.Timeout;
import scala.concurrent.Await;
import worker.config.ContantConfig;
import worker.messages.*;

import java.time.Duration;
import java.util.concurrent.CompletionStage;

import scala.concurrent.Future;
import worker.modal.OrdersModal;
import worker.modal.WorkerModal;

import static akka.http.javadsl.server.PathMatchers.segment;
import static java.util.regex.Pattern.compile;


public class WorkerServer extends AllDirectives {
    private Duration TIMEOUT = Duration.ofSeconds(5l);
    Timeout timeout = Timeout.create(Duration.ofSeconds(5));

    //contants
    static ContantConfig myContants = new ContantConfig();

    public static void main(String[] args) {
        ActorSystem system = ActorSystem.create("worker-service");

        Http http = Http.get(system);
        ActorMaterializer materializer = ActorMaterializer.create(system);

        WorkerServer app = new WorkerServer(system);

        Flow<HttpRequest, HttpResponse, NotUsed> routeFlow = app.createRoute().flow(system, materializer);

        http.bindAndHandle(routeFlow, ConnectHttp.toHost("localhost", myContants.getPort()), materializer);
        System.out.println("Server started on port: " + myContants.getPort());
    }


    private ActorRef worker;

    private NotificationUrlList result = null;


    public WorkerServer(ActorSystem system) {

        worker = system.actorOf(WorkerManager.props());
//        worker.tell(new Init(worker), null);


        Future<Object> future = Patterns.ask(worker, new Init(worker), timeout);


        try {
            result = (NotificationUrlList) Await.result(future, timeout.duration());
        } catch (Exception e) {

        }


    }

    protected Route createRoute() {


        return route(
                path(segment(myContants.getRest_postNotify()).slash(segment()), (i) ->
                        handleNotification(i)),

                concat(
                        pathPrefix(myContants.getRest_managment(), () ->
                                concat(
                                        pathEnd(() -> handleWorkersList()),
                                        path(segment(), (i) ->
                                                handleOrderList(i))
                                ))
                )

        );
    }


    private Route handleNotification(String name) {
        return route(
                post(() ->
                        entity(Jackson.unmarshaller(ActionReceive.class), actionReceive -> {
                            CompletionStage<Result> notify
                                    = Patterns.ask(worker,
                                    new NotifyWorker(name,actionReceive), TIMEOUT
                            ).thenApply(Result.class::cast);

                            return onSuccess(() -> notify,
                                    msg -> {
                                        if (msg.isSuccess())
                                            return complete(
                                                    StatusCodes.OK,
                                                    msg, Jackson.marshaller()
                                            );
                                        else
                                            return complete(
                                                    StatusCodes.NOT_FOUND,
                                                    msg, Jackson.marshaller()
                                            );

                                    });
                        })
                )
        );
    }


    private Route handleWorkersList() {
        return route(
                get(() -> {
                            CompletionStage<GetWorkerResult> notify
                                    = Patterns.ask(worker,
                                    "getWorker", TIMEOUT
                            ).thenApply(GetWorkerResult.class::cast);

                            return onSuccess(() -> notify,
                                    msg -> {
                                        if (msg.isSuccess())
                                            return complete(
                                                    StatusCodes.OK,
                                                    msg, Jackson.marshaller()
                                            );
                                        else
                                            return complete(
                                                    StatusCodes.NOT_FOUND,
                                                    msg, Jackson.marshaller()
                                            );
                                    });
                        }
                )
        );
    }


    private Route handleOrderList(String name) {
        return route(
                post(() ->
                        entity(Jackson.unmarshaller(OrderReq.class), ackNotify -> {
                            CompletionStage<Result> notify
                                    = Patterns.ask(worker,
                                    new WorkerOrder(name, ackNotify.getOrder()), TIMEOUT
                            ).thenApply(Result.class::cast);

                            return onSuccess(() -> notify,
                                    msg -> {
                                        if (msg.isSuccess())
                                            return complete(
                                                    StatusCodes.OK,
                                                    msg, Jackson.marshaller()
                                            );
                                        else
                                            return complete(
                                                    StatusCodes.NOT_FOUND,
                                                    msg, Jackson.marshaller()
                                            );
                                    });
                        })
                )
        );
    }


}
